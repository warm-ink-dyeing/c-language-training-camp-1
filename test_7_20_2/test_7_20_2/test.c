//#include <stdio.h>

//main函数 - 主函数
//main函数是程序的入口，程序都是从main函数的第一行开始执行
//
//int main()
//{
//	printf("hello C\n");
//	printf("hello C\n");
//	printf("hello C\n");
//	printf("hello C\n");
//	printf("hello C\n");
//	printf("hello C\n");
//	printf("hello C\n");
//
//	return 0;
//}

//背景颜色修改-一会儿讲

//int - 整型
//模板
//int main()
//{
//	//写代码
//	return 0;
//}

//int main()
//{
//	printf("hehe\n");
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("呵呵\n");
//	return 0;
//}

//#include <stdio.h>
////standard input output
////标准输入输出
////
//int main()
//{
//	printf("hehe\n");//\n 换行
//	printf("abcdef\n");
//	printf("bbq\n");
//	printf("%d\n", 100);//%d 是打印整型的
//	printf("%c\n", 'w');//%c 是打印字符的
//	printf("%f\n", 3.5f);//%f 是打印小数
//	//占位符 - 
//
//	return 0;
//}


#include <stdio.h>


//int main()
//{
//	//32-127
//	int i = 0;
//	for (i = 32; i <= 127; i++)
//	{
//		if (i % 16 == 0)
//			printf("\n");
//		printf("%c ", i);
//	}
//	return 0;
//}


//int main()
//{
//	printf("%d\n", 100);
//	printf("%c\n", 100);
//
//	return 0;
//}

//
//int main()
//{
//	'a';
//	'w';//字符
//	"abc";//字符串
//	"a";
//	"";//空字符串
//
//	return 0;
//}
//

//int main()
//{
//	printf("hehe\n");
//	printf("%s", "haha");//%s 是用来打印字符串的
//
//	return 0;
//}

//
//int main()
//{
//	//"abcdef";//字符串的末尾隐藏一个\0字符
//	//\0 是字符串的结束标注
//
//	//字符数组
//	//数组 - 一组数据
//	//'a', 'b' - 字符
//	char arr1[] = "abc";
//	char arr2[] = {'a', 'b', 'c'};
//	char arr3[] = "abc\0def";
//
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//
//	return 0;
//}
//

//int main()
//{
//	printf("abc\ndef");
//
//	return 0;
//}

//int main()
//{
//	//printf("%c", 'x');
//	//printf("%c", '\'');
//
//	printf("%s\n", "abc");
//	printf("%s\n", "a");
//	printf("%s\n", "\"");
//
//
//	return 0;
//}

//int main()
//{
//	//printf("c:\\test\\code\\test.c");
//	//c:\test\code\test.c
//	//printf("\a");
//	//printf("abc\bdef");
//	printf("ab\tdef\tq\txxxxx");
//
//	return 0;
//}


int main()
{
	//printf("%c\n", '\130');
	printf("%c\n", '\x30');

	return 0;
}
